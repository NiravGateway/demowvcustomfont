//
//  ViewController.swift
//  DemoWVCustomFont
//
//  Created by Nirav Jariwala on 19/07/21.
//

import UIKit
import WebKit

class ViewController: UIViewController,WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadWebview()
    }

    func loadWebview() {
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        let urlToload = Bundle.main.url(forResource: "Data", withExtension: "html")
        webView.loadFileURL(urlToload!, allowingReadAccessTo: urlToload!)
        let req = URLRequest(url: urlToload!)
        webView.load(req)
        
        //let url = Bundle.main.url(forResource: "Data", withExtension: "html", subdirectory: "Website")
        //webView.loadFileURL(url, allowingReadAccessTo: url)
        //let request = URLRequest(url: url)
        //webView.load(request)
    }

}

